/* SPDX-License-Identifier: Apache-2.0 */
/*
 * Copyright (C) 2017 Intel Corporation.
 *
 * IPAIPU3Stats.cpp: Generate statistics in IA AIQ consumable format.
 */

#ifndef IPA_IPU3_STATS_H
#define IPA_IPU3_STATS_H

#include <linux/intel-ipu3.h>
#include "aiq/aiq_results.h"

namespace libcamera::ipa::ipu3 {

constexpr uint32_t IPU3MaxStatisticsWidth = 80;
constexpr uint32_t IPU3MaxStatisticsHeight = 60;
constexpr uint32_t IPU3MaxStatisticsGridSize =
	IPU3MaxStatisticsWidth * IPU3MaxStatisticsHeight;

struct AiqResults;

class IPAIPU3Stats
{
public:
	IPAIPU3Stats() = default;
	~IPAIPU3Stats() = default;

	ia_aiq_statistics_input_params* getInputStatsParams(
			int frame,
			aiq::AiqResults *results,
			const ipu3_uapi_stats_3a *stats);

private:
	ia_aiq_statistics_input_params aiqStatsInputParams_ = {};

	/*!< ia_aiq_statistics_input_params pointer contents */
	const ia_aiq_rgbs_grid* rgbsGridPtr_ = nullptr;
	const ia_aiq_af_grid* afGridPtr_ = nullptr;
	ia_aiq_rgbs_grid rgbsGrid_ = {};
	ia_aiq_af_grid afGrid_ = {};

	/*!< ia_aiq_rgbs_grid pointer contents */
	rgbs_grid_block rgbsGridBlock_[IPU3MaxStatisticsGridSize] = {};

	/*!< ia_aiq_af_grid pointer contents */
	int filterResponse1_[IPU3MaxStatisticsGridSize] = {};
	int filterResponse2_[IPU3MaxStatisticsGridSize] = {};
};

} /* namespace libcamera::ipa::ipu3 */

#endif /* IPA_IPU3_STATS_H */

