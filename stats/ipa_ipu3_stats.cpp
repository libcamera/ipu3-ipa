/* SPDX-License-Identifier: Apache-2.0 */
/*
 * Copyright (C) 2017 Intel Corporation.
 *
 * IPAIPU3Stats.cpp: Generate statistics in IA AIQ consumable format.
 */

#include "ipa_ipu3_stats.h"

#include <libcamera/base/log.h>

#include "ipu3_all_stats.h"

namespace libcamera::ipa::ipu3 {

LOG_DEFINE_CATEGORY(IPAIPU3Stats)

ia_aiq_statistics_input_params *
IPAIPU3Stats::getInputStatsParams(int frame, aiq::AiqResults *results,
				  const ipu3_uapi_stats_3a *stats)
{
	IPU3AllStats::ipu3_stats_all_stats outStats = {};
	IPU3AllStats::ipu3_stats_get_3a(&outStats, stats);

	rgbsGrid_.blocks_ptr = rgbsGridBlock_;

	afGrid_.filter_response_1 = filterResponse1_;
	afGrid_.filter_response_2 = filterResponse2_;

	IPU3AllStats::intel_skycam_statistics_convert(
			outStats.ia_css_4a_statistics, &rgbsGrid_, &afGrid_);

	aiqStatsInputParams_ = {};
	aiqStatsInputParams_.frame_id = frame;
	aiqStatsInputParams_.frame_ae_parameters = results->ae();
	aiqStatsInputParams_.frame_af_parameters = results->af();
	aiqStatsInputParams_.awb_results = results->awb();
	aiqStatsInputParams_.frame_pa_parameters = results->pa();
	aiqStatsInputParams_.frame_sa_parameters = results->sa();
	aiqStatsInputParams_.camera_orientation = ia_aiq_camera_orientation_unknown;

	rgbsGridPtr_ = &rgbsGrid_;
	aiqStatsInputParams_.num_rgbs_grids = 1;
	aiqStatsInputParams_.rgbs_grids = &rgbsGridPtr_;

	afGridPtr_ = &afGrid_;
	aiqStatsInputParams_.num_af_grids = 1;
	aiqStatsInputParams_.af_grids = &afGridPtr_;

	aiqStatsInputParams_.hdr_rgbs_grid = nullptr;
	aiqStatsInputParams_.depth_grids = nullptr;

	aiqStatsInputParams_.num_external_histograms = 0;
	aiqStatsInputParams_.external_histograms = nullptr;

	/* \todo: Fill the face state after the integration of face detection. */
	aiqStatsInputParams_.faces = nullptr;

	return &aiqStatsInputParams_;
}

} /* namespace libcamera::ipa::ipu3 */
